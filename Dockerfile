#
# Build stage
#
FROM maven:3.9.1-amazoncorretto-17 AS build
COPY . .
RUN mvn clean package -DskipTests

#
# Package stage
#
FROM amazoncorretto:17.0.7-alpine3.14
COPY --from=build /target/loja-0.0.1-SNAPSHOT.jar loja.jar
# ENV PORT=8080
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "loja.jar"]
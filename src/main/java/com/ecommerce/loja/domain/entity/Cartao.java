package com.ecommerce.loja.domain.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Cartao extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String ultimosNumeros;

    private boolean valido;

    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "id")
    private Usuario usuario;

}

package com.ecommerce.loja.domain.entity;

import jakarta.persistence.*;
import org.hibernate.validator.constraints.Length;

@Entity
public class Categoria extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Length(max = 32)
    private String nome;

    @Length(max = 255)
    private String descricao;

}

package com.ecommerce.loja.domain.dto;

import com.ecommerce.loja.domain.entity.Produto;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class CarrinhoDTO {

    private Long usuarioId;

    @ToString.Include
    private List<Produto> produtos;

}

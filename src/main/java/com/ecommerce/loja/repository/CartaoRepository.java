package com.ecommerce.loja.repository;

import com.ecommerce.loja.domain.entity.Cartao;
import com.ecommerce.loja.domain.entity.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CartaoRepository extends JpaRepository<Cartao, Long>, JpaSpecificationExecutor<Cartao> {}

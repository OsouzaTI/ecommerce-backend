package com.ecommerce.loja.repository;

import com.ecommerce.loja.domain.entity.Produto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {
    Optional<Produto> findByApiId(Long id);
}

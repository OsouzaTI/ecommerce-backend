package com.ecommerce.loja.repository.specs;

import com.ecommerce.loja.domain.entity.Cartao;
import com.ecommerce.loja.domain.entity.Usuario;
import jakarta.persistence.criteria.Join;
import org.springframework.data.jpa.domain.Specification;

public class CartaoSpec {

    public static Specification<Cartao> cartaoValidoUsuario(Usuario usuario) {
        return (root, query, criteriaBuilder) -> {
            Join<Cartao, Usuario> usuarioCartoes = root.join("usuario");
            return criteriaBuilder.and(
                criteriaBuilder.equal(usuarioCartoes.get("id"), usuario.getId()),
                criteriaBuilder.isTrue(root.get("valido"))
            );
        };
    }

}

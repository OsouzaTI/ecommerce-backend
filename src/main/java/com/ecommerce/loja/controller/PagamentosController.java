package com.ecommerce.loja.controller;

import com.ecommerce.loja.repository.PagamentoRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pagamentos")
public class PagamentosController {

    private final PagamentoRepository pagamentoRepository;

    public PagamentosController(PagamentoRepository pagamentoRepository) {
        this.pagamentoRepository = pagamentoRepository;
    }

    @GetMapping("listar")
    public ResponseEntity<?> listar() {
        return ResponseEntity.ok(pagamentoRepository.findAll());
    }

}

package com.ecommerce.loja.controller;

import com.ecommerce.loja.domain.dto.CarrinhoDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.pool.HikariProxyCallableStatement;
import jakarta.validation.Valid;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

@RestController
@RequestMapping("/carrinho")
public class CarrinhoController {

    private final RabbitTemplate rabbitTemplate;

    public CarrinhoController(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @PostMapping("/comprar")
    public ResponseEntity<?> carrinho(@Valid @RequestBody CarrinhoDTO carrinhoDTO) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        String jsonCarrinhoDTO = mapper.writeValueAsString(carrinhoDTO);

        Message message = MessageBuilder
                .withBody(jsonCarrinhoDTO.getBytes())
                .setContentType(MessageProperties.CONTENT_TYPE_JSON)
                .build();

        rabbitTemplate.convertAndSend("carrinho_queue", message);
        return ResponseEntity.status(200).build();
    }

}

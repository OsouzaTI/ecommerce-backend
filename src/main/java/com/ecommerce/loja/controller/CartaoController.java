package com.ecommerce.loja.controller;

import com.ecommerce.loja.domain.entity.Cartao;
import com.ecommerce.loja.domain.entity.Usuario;
import com.ecommerce.loja.repository.CartaoRepository;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    private final CartaoRepository cartaoRepository;

    public CartaoController(CartaoRepository cartaoRepository) {
        this.cartaoRepository = cartaoRepository;
    }

    @PostMapping("/criar")
    public ResponseEntity<?> criar(@Valid @RequestBody Cartao cartao) {
        // salvando usuario
        cartaoRepository.save(cartao);
        return ResponseEntity.status(200).build();
    }

}

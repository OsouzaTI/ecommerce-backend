package com.ecommerce.loja.controller;

import com.ecommerce.loja.domain.entity.Usuario;
import com.ecommerce.loja.repository.CartaoRepository;
import com.ecommerce.loja.repository.UsuarioRepository;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    private final UsuarioRepository usuarioRepository;
    private final CartaoRepository cartaoRepository;

    public UsuarioController(UsuarioRepository usuarioRepository, CartaoRepository cartaoRepository) {
        this.usuarioRepository = usuarioRepository;
        this.cartaoRepository = cartaoRepository;
    }

    @PostMapping("/criar")
    public ResponseEntity<?> criar(@Valid @RequestBody Usuario usuario) {
        // salvando usuario
        usuarioRepository.save(usuario);
        return ResponseEntity.status(200).build();
    }

}

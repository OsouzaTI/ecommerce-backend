package com.ecommerce.loja.configuration;

import com.ecommerce.loja.domain.dto.CarrinhoDTO;
import com.ecommerce.loja.domain.entity.Cartao;
import com.ecommerce.loja.domain.entity.Pagamento;
import com.ecommerce.loja.domain.entity.Produto;
import com.ecommerce.loja.domain.entity.Usuario;
import com.ecommerce.loja.repository.CartaoRepository;
import com.ecommerce.loja.repository.PagamentoRepository;
import com.ecommerce.loja.repository.ProdutoRepository;
import com.ecommerce.loja.repository.UsuarioRepository;
import com.ecommerce.loja.repository.specs.CartaoSpec;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class RabbitMQListener {

    private final CartaoRepository cartaoRepository;
    private final UsuarioRepository usuarioRepository;

    private final PagamentoRepository pagamentoRepository;
    private final ProdutoRepository produtoRepository;

    public RabbitMQListener(CartaoRepository cartaoRepository, UsuarioRepository usuarioRepository, PagamentoRepository pagamentoRepository, ProdutoRepository produtoRepository) {
        this.cartaoRepository = cartaoRepository;
        this.usuarioRepository = usuarioRepository;
        this.pagamentoRepository = pagamentoRepository;
        this.produtoRepository = produtoRepository;
    }

    @RabbitListener(queues = "carrinho_queue")
    public void handleMessage(String message) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        CarrinhoDTO carrinho = mapper.readValue(message, CarrinhoDTO.class);

        System.out.println("Carrinho de compras recebido...: " + carrinho);

        // processando compra para verificar se sera aceita ou nao
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(carrinho.getUsuarioId());
        if (usuarioOptional.isPresent()) {

            // buscando cartoes validos
            Usuario usuario = usuarioOptional.get();
            Specification<Cartao> specs = CartaoSpec.cartaoValidoUsuario(usuario);
            List<Cartao> cartoes = cartaoRepository.findAll(specs);
            if (!cartoes.isEmpty()) {
                for (Produto produto : carrinho.getProdutos()) {
                    Pagamento pagamento = new Pagamento();

                    // buscando produto
                    Optional<Produto> produtoOptional = produtoRepository.findByApiId(produto.getApiId());
                    if (produtoOptional.isPresent()) {
                        produto = produtoOptional.get();
                    } else {
                        produtoRepository.save(produto);
                    }

                    pagamento.setProduto(produto);
                    pagamento.setQuantidade(1);
                    pagamento.setUsuario(usuario);
                    pagamentoRepository.save(pagamento);
                }
            }

        }

    }

}

